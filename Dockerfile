FROM perl:latest

LABEL maintainer="tomascohen@theke.io"

RUN cpanm --notest Modern::Perl Slack::RTM::Bot YAML::XS Text::CSV::Slurp LWP::Simple Data::Printer

WORKDIR /app
COPY . .

CMD PERL5LIB=. perl bot.pl
