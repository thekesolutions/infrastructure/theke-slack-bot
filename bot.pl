#!/usr/bin/env perl

=head1 NAME

bot - A Slackbot for Theke Solutions

=cut

use feature qw(say);

use Modern::Perl;

use List::Util qw(shuffle);
use Slack::RTM::Bot;
use YAML::XS qw(LoadFile DumpFile Load);
use JSON;
use Text::CSV::Slurp;
use LWP::Simple;
use IO::Socket::SSL qw/SSL_VERIFY_NONE/;
use Try::Tiny;

my $slack_bot_token = $ENV{SLACK_BOT_TOKEN};
my $main_channel    = $ENV{MAIN_CHANNEL} // 'general';
my $debug           = $ENV{DEBUG} || 0;

say "Theke bot is starting!";

die "No SLACK_BOT_TOKEN set!" unless $slack_bot_token;

my $bot = Slack::RTM::Bot->new( token => $slack_bot_token );
my $ua  = LWP::UserAgent->new(
    ssl_opts => {
        verify_hostname => 0,
        SSL_verify_mode => SSL_VERIFY_NONE
    }
);
$ua->agent('ThekeBot/Slack::RTM::Bot');

=head1 Capabilities

=head2 Zendesk

    Converts Zendesk ticket numbers into URLs.
    Can be of the form "ticket 1234" or "rt 1234".
    The keywords are case insenstivie.

=cut

my $regex_rt = qr/(ticket)\s*([0-9]+)/m;

$bot->on(
    { text => $regex_rt },
    sub {
        my ($response) = @_;
        warn "handle_ticket_numbers" if $debug;

        return unless $response->{channel};

        $response->{text} =~ $regex_rt;
        my $ticket = $2;

        my $url  = "https://theke.zendesk.com/agent/tickets/$ticket";
        my $text = "Zendesk ticket <$url|$ticket>";

        $bot->say(
            channel => $response->{channel},
            text    => $text,
            mrkdwn  => 1,
        );
    }
);

=head2 Koha's Bugzilla

    Converts Koha community bug numbers into URLs.
    Can be of the form "bug 1234" or "bz 1234".
    The keywords are case insenstivie.

=cut

my $regex_bz = qr/(bug|bz)\s*([0-9]+)/m;

$bot->on(
    { text => $regex_bz },
    sub {
        my ($response) = @_;
        warn "handle_bug_numbers" if $debug;

        return unless $response->{channel};

        $response->{text} =~ $regex_bz;
        my $bug = $2;

        my $url = "https://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=$bug";

        my $res      = decode_json( $ua->get("https://bugs.koha-community.org/bugzilla3/rest/bug/$bug")->content );
        my $bug_data = $res->{bugs}[0];

        my $text = "Koha community <$url|Bug $bug>: _" . $bug_data->{summary} . "_ [*" . $bug_data->{status} . "*]";

        $bot->say(
            channel => $response->{channel},
            text    => $text,
            mrkdwn  => 1,
        );
    }
);

$bot->start_RTM( \&handle_startup );

sub handle_startup {

    while (1) {
        sleep 5;
        try {
            $bot->{client}->find_conversation_id($main_channel);
        } catch {
            print STDERR "Reconnecting: $_";
            $bot->start_RTM( \&handle_startup );
        };
    }
}

1;
